import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {

    private PetRock rocky;

    @BeforeEach
    public void setup(){
        rocky = new PetRock("Rocky");
    }

    @Test
    void getName() {
        //PetRock rocky = new PetRock("Rocky");
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void testUnhappyToStart() {
        //PetRock rocky = new PetRock("Rocky");
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled ("Exception throwing not yet defined")
    @Test
    void nameFail() {
        assertThrows(IllegalStateException.class, () -> rocky.getHappyMessage());
    }

    @Test
    void name() {
        rocky.playWithRock();
        assertEquals("I'm happy", rocky.getHappyMessage());
    }

    @Test
    void testFavNum(){
        assertEquals(42, rocky.getFavNumber());
    }

    @Test
    void emptyNameFail(){
        assertThrows(IllegalArgumentException.class, () -> new PetRock(""));
    }

}